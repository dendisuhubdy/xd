#include "alternatives.ih"

bool Alternatives::matchIgnore(std::string const &ignore, string const &entry)
{
    // returns true if entry matches an ignore string
    return
            *ignore.rbegin() != '*' ?       // literal match required
                ignore == entry
            :                               // wildcard match of final * OK
                entry.find(ignore.substr(0, ignore.length() - 1)) == 0;
}
