#include "main.ih"               // program header file

int main(int argc, char **argv)
try
{
    arguments(argc, argv);

    Alternatives alternatives;
                                        // select viable alternatives
    if (alternatives.viable() == Alternatives::ONLY_CD)
        return 0;

    alternatives.order();               // history alternatives first or last

    Filter filter(alternatives);

    filter.select();                   // make the selection

    return filter.decided() ? 0 : 1;   // return 0 to the OS if the filter
                                        // did do its work
}
catch(exception const &err)     // handle exceptions
{
    cerr << err.what() << endl;
    cout << ".\n";          // to prevent a directory change
    return 1;
}
catch(int x)
{
    if (x == 0)
        cerr << "No Solutions\n";

    if (ArgConfig::instance().option("hv"))
        return 0;

    cout << ".\n";
    return x;
}
